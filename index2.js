    let userName;
    let password;
    let role;


    function userLogin(
            inputUserName = prompt("Please input your Username:"),
            inputPassword = prompt("Please input your Password:"),
            inputRole = prompt("Please input your Role").toLowerCase()
        ){

        if(
        inputUserName === null 
        || inputUserName === "" 
        || inputPassword === null
        || inputPassword === ""
        || inputRole === null
        || inputRole === ""
        ){
            alert("Please don't leave inputs empty");
        }else{

            userName = inputUserName;
            password = inputPassword;
            role = inputRole;

            switch(inputRole){
            case "admin":
                alert("Welcome back to the class portal, admin!");
                break;
            case "teacher":
                alert("Thank you for logging in, teacher!");
                break;
            case "student":
                alert("Welcome to the class portal, student!");
                break;
            default:
                alert("Role out of range.");
            }

        }
    }

    userLogin();

    function convertNumToLetter(firstNumber, secondNumber, thirdNumber, fourthNumber){

        if(role === "teacher" || role === "admin" || role === null || role === "" || typeof role === "undefined"){
            alert(role + "! You are not allowed to access this feature!");
        }else{
            let average = Math.round((firstNumber + secondNumber + thirdNumber + fourthNumber) / 4);
            console.log(average);
            if(average <= 74){
                console.log("Hello, student, your average is " + average + ". The letter equivalent is F")
            }else if(average >= 75 && average <= 79){
                console.log("Hello, student, your average is " + average + ". The letter equivalent is D")
            }else if(average >= 80 && average <= 84){
                console.log("Hello, student, your average is " + average + ". The letter equivalent is C")
            }else if(average >= 85 && average <= 89){
                console.log("Hello, student, your average is " + average + ". The letter equivalent is B")
            }else if(average >= 90 && average <= 95){
                console.log("Hello, student, your average is " + average + ". The letter equivalent is A")
            }else if(average >= 96){
               console.log("Hello, student, your average is " + average + ". The letter equivalent is A+")
            }
        }

    }